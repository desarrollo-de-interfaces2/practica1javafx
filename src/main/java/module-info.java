module com.example.practica1javafx {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.practica1javafx to javafx.fxml;
    exports com.example.practica1javafx;
}