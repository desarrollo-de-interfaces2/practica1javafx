package com.example.practica1javafx;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
public class HelloController {
    @FXML
    private Button bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, btBarra, btC, btIgual, btMenos, btPunto, btSuma, btX;
    @FXML
    private TextField tvcalc;
    private String cal = "";
    private double numero = 0;
    private double total = 0;
    private boolean coma = false;

    @FXML
    void handleButtonAction(ActionEvent event) {
        //System.out.println(((Control) event.getSource()).getId());

        switch (((Control)event.getSource()).getId()){
            case "bt1":
                tvcalc.setText(tvcalc.getText() + "1");
                break;
            case "bt2":
                tvcalc.setText(tvcalc.getText() + "2");
                break;
            case "bt3":
                tvcalc.setText(tvcalc.getText() + "3");
                break;
            case "bt4":
                tvcalc.setText(tvcalc.getText() + "4");
                break;
            case "bt5":
                tvcalc.setText(tvcalc.getText() + "5");
                break;
            case "bt6":
                tvcalc.setText(tvcalc.getText() + "6");
                break;
            case "bt7":
                tvcalc.setText(tvcalc.getText() + "7");
                break;
            case "bt8":
                tvcalc.setText(tvcalc.getText() + "8");
                break;
            case "bt9":
                tvcalc.setText(tvcalc.getText() + "9");
                break;
            case "bt0":
                tvcalc.setText(tvcalc.getText() + "0");
                break;
            case "btPunto":
                char a = ' ';
                for(int i = 0;i < tvcalc.getLength(); i++){
                    a = tvcalc.getText().charAt(i);
                    if(a == '.'){
                        coma = true;
                    }
                }
                if(coma){
                    tvcalc.setText(tvcalc.getText() + "");
                }else{
                    tvcalc.setText(tvcalc.getText() + ".");
                }
                break;
            case "btIgual":
                if(cal.equals("+")){
                    numero = Double.parseDouble(tvcalc.getText());
                    total += numero;
                    tvcalc.setText(String.valueOf(total));
                }else if(cal.equals("-")){
                    numero = Double.parseDouble(tvcalc.getText());
                    total -= numero;
                    System.out.println(numero);
                    tvcalc.setText(String.valueOf(total));
                }else if(cal.equals("*")){
                    numero = Double.parseDouble(tvcalc.getText());
                    total *= numero;
                    tvcalc.setText(String.valueOf(total));
                }else if(cal.equals("/")){
                    numero = Double.parseDouble(tvcalc.getText());
                    total /= numero;
                    tvcalc.setText(String.valueOf(total));
                }
                break;
            case "btSuma":
                numero = Double.parseDouble(tvcalc.getText());
                if(total == 0){
                    total = numero;
                }else if(cal.equals("/")){
                    total /= numero;
                }else if(cal.equals("-")){
                    total -= numero;
                }else if(cal.equals("*")){
                    total *= numero;
                }else{
                    total += numero;
                }
                tvcalc.setText("");
                cal = "+";
                break;
            case "btMenos":
                numero = Double.parseDouble(tvcalc.getText());
                if(total == 0){
                    total = numero;
                }else if(cal.equals("+")){
                    total += numero;
                }else if(cal.equals("/")){
                    total /= numero;
                }else if(cal.equals("*")){
                    total *= numero;
                }else{
                    total -= numero;
                }
                System.out.println(total);
                tvcalc.setText("");
                cal = "-";
                break;
            case "btC":
                numero = 0;
                total = 0;
                cal = null;
                tvcalc.setText("");
                break;
            case "btBarra":
                numero = Double.parseDouble(tvcalc.getText());
                if(total == 0){
                    total = numero;
                }else if(cal.equals("+")){
                    total += numero;
                }else if(cal.equals("-")){
                    total -= numero;
                }else if(cal.equals("*")){
                    total *= numero;
                }else{
                    total /= numero;
                }
                tvcalc.setText("");
                cal = "/";
                break;
            case "btX":
                numero = Double.parseDouble(tvcalc.getText());
                if(total == 0){
                    total = numero;
                }else if(cal.equals("+")){
                    total += numero;
                }else if(cal.equals("/")){
                    total /= numero;
                }else if(cal.equals("-")){
                    total -= numero;
                }else{
                    total *= numero;
                }
                System.out.println(total);
                tvcalc.setText("");
                cal = "*";
                break;
        }
    }
}